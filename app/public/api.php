<?php

/**
 * Contedia API request hander script.
 */

require_once '../src/config.php';
require_once '../src/classes/DB.php';
require_once '../src/classes/Submission.php';

$response = [
    'success' => null,
];
$errs = array();

$firstName  = trim($_POST['firstName']);
$lastName   = trim($_POST['lastName']);
$telNo      = trim($_POST['telephoneNo']);
$email      = trim($_POST['email']);
$subject    = trim($_POST['subject']);
$car        = trim($_POST['car']);
$bike       = trim($_POST['motorbike']);
$fuel       = trim($_POST['fuel']);
$comment    = trim($_POST['comment']);
$gdpr       = boolval($_POST['gdpr']);


$textRegex = "/^[a-z-' wàâáçéèèêëìîíïôòóùûüÂÊÎÔúÛÄËÏÖÜÀÆæÇÉÈŒœÙñý]*$/mi";
$telRegex  = "/^[+0-9]{1,4}(-| ){0,1}[0-9]{1,4}(-| ){0,1}[0-9]{1,4}(-| ){0,1}[0-9]{1,4}$/";

$nameMaxLen = 100;
$commentMaxLen = 500;

$carWL = [
    'volvo',
    'tesla',
    'mercedes',
];
$bikeWL = [
    'suzuki',
    'harley',
    'triumph',
];
$fuelWL = [
    'kerosene',
    'petrol',
    'cooking-oil',
    'diesel',
];

if (empty($firstName)) {
    $errs['first_name'] = "Please enter a fist name.";
} else if (!preg_match($textRegex, $firstName)) {
    $errs['first_name'] = "Fist name is invalid.";
} else if (strlen($firstName) > $nameMaxLen) {
    $errs['first_name'] = "Fist name is too long.";
}

if (!preg_match($textRegex, $lastName)) {
    $errs['last_name'] = "Fist name is invalid.";
} else if (strlen($lastName) > $nameMaxLen) {
    $errs['last_name'] = "Fist name is too long.";
}

if (empty($email)) {
    $errs['email'] = "Please enter an email address.";
} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errs['email'] = 'Email address is invalid.';
}

if (empty($subject)) {
    $errs['subject'] = "Please enter a subject.";
} else if (!preg_match($textRegex, $subject)) {
    $errs['subject'] = "Subject is invalid.";
} else if (strlen($subject) > $nameMaxLen) {
    $errs['subject'] = "Subject is too long.";
}

if (empty($telNo)) {
    $errs['telephone_no'] = "Please enter a telephone number.";
} else if (!preg_match($telRegex, $telNo)) {
    $errs['telephone_no'] = 'Telephone number is invalid.';
}

if (!in_array($car, $carWL)) {
    $errs['car'] = 'Car selection is invalid.';
}

if (!in_array($bike, $bikeWL)) {
    $errs['bike'] = 'Bike is invalid.';
}

if (!in_array($fuel, $fuelWL)) {
    $errs['fuel'] = 'Please select a fuel.';
}

if (!preg_match($textRegex, $comment)) {
    $errs['comment'] = "Your comment submission was invalid.";
} else if (strlen($comment) > $commentMaxLen) {
    $errs['comment'] = "Your comment is too long.";
}

if ($gdpr === false) {
    $errs['gdpr'] = 'You must agree to our GDPR policy.';
}

if (!empty($errs)) {
    $response['success'] = false;
    $response['errors'] = $errs;
    echo json_encode($response);
    die();
}

$response['success'] = true;
$subModel = new Submission;
$sub = array(
    'first_name'    => filter_var($firstName, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'last_name'     => filter_var($lastName, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'email'         => filter_var($email, FILTER_SANITIZE_EMAIL),
    'subject'       => filter_var($subject, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'telephone_no'  => filter_var($telNo, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'car'           => filter_var($car, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'bike'          => filter_var($bike, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'fuel'          => filter_var($fuel, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'comment'       => filter_var($comment, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
    'gdpr'          => filter_var($gdpr, FILTER_SANITIZE_NUMBER_INT),
);

$subModel->add_sub($sub);

echo json_encode($response);