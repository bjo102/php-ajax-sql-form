/**
 * Contedia Form Modules: FormSub.
 * 
 * @package Contedia_Form\FormSub
 */


const FormSub = e => {
    // console.log(e.target)
    e.preventDefault();

    const URL = "http://localhost:8000/api.php";

    const form = e.target;

    let selectedFuel = null;

    let gdpr = form.querySelector('#accept-gdpr').checked ? 1 : 0;

    for (const radioButton of Array.from(form.querySelectorAll('input[name="fuel"]'))) {
        if (radioButton.checked) {
            selectedFuel = radioButton.value;
            break;
        }
    }

    var formData = new FormData();
    formData.append('firstName',    form.querySelector('#first-name').value);
    formData.append('lastName',     form.querySelector('#last-name').value);
    formData.append('email',        form.querySelector('#email').value);
    formData.append('subject',      form.querySelector('#subject').value);
    formData.append('telephoneNo',  form.querySelector('#telephone-no').value);
    formData.append('car',          form.querySelector('#cars-select-wrapper .custom-option.selected').dataset.value);
    formData.append('motorbike',    form.querySelector('#motorbike-select-wrapper .custom-option.selected').dataset.value);
    formData.append('fuel',         selectedFuel);
    formData.append('comment',      form.querySelector('#comment').value);
    formData.append('gdpr',         gdpr);

    fetch( URL, {
        method: 'POST',
        body: formData
    })
    .then(response => {
        return response.json();
    })
    .then(json => {
        if(json.success) {
            form.reset();
            clearErrs();
        } else {
            clearErrs();
            Object.keys(json.errors).map(err => {
                let errMsg = json.errors[err];
                let errNote = form.querySelector(`#err-note-${err}`);
                console.log(errNote);
                errNote.classList.add('has-error');
                errNote.textContent = errMsg;
            })
        }
        console.log(json);
    });


    function clearErrs() {
        Array.from(form.querySelectorAll('.err-note')).map(errNote => {
            errNote.classList.remove('has-error');
            errNote.textContent = '';
        })
    }

    /* async function formNotice(msg) {
        const wait = time => new Promise(tick => setTimeout(tick, time));
        const fNote = form.querySelector('#form-submission-notice');
        fNote.textContent = msg;
        await wait(10);
        fNote.classList.add('after-submission');
        await wait(4000);
        fNote.classList.remove('after-submission');
        await wait(3200);
        fNote.textContent = '';
    } */
    
};

export default FormSub;