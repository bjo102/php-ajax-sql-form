/**
 * Contedia Form Modules: SelectField.
 * 
 * @package Contedia_Form\SelectField
 */

/**
 * Set behaviour for custom select boxes.
 * 
 * @param {object} formEl The form to search for fields 
 * @param {string} fieldId The ID of the field to find
 */
const SelectField = (formEl, fieldId) => {

    const selectWrapper = formEl.querySelector(`#${fieldId}`)

    selectWrapper.addEventListener('click', function() {
        this.querySelector('.select').classList.toggle('open');
    });
    
    for (const option of document.querySelectorAll(".custom-option")) {
        option.addEventListener('click', function() {
            if (!this.classList.contains('selected')) {
                this.parentNode.querySelector('.custom-option.selected').classList.remove('selected');
                this.classList.add('selected');
                this.closest('.select').querySelector('.select__trigger span').textContent = this.textContent;
            }
        });
    }
    
    window.addEventListener('click', e => {
        const select = document.querySelector(`#${fieldId} .select`)
        if (!select.contains(e.target)) {
            select.classList.remove('open');
        }
    });
}

export default SelectField;