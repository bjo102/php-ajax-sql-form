/**
 * Contedia Form Modules: ContediaForm.
 * 
 * @package Contedia_Form\Modules
 */

"use strict";

import SelectField from './modules/SelectField.mjs';
import FormSub from './modules/FormSub.mjs';

window.onload = () => ContediaForm();

/**
 * Apply form functionalities
 */
function ContediaForm() {

    const contediaForm = document.getElementById('contedia-form');

    SelectField(contediaForm, 'cars-select-wrapper');
    SelectField(contediaForm, 'motorbike-select-wrapper');
    contediaForm.addEventListener('submit', FormSub)
};