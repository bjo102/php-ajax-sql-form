<?php

require_once 'config.php';

require_once 'classes/DB.php';
require_once 'classes/Submission.php';

require_once 'theme/header.php';
require_once 'theme/form.php';
require_once 'theme/footer.php';