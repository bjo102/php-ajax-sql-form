<?php

/**
 * Contedia Views: Header
 * 
 * @package Contedia\Views
 */

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
            name="description"
            content="Contedia form page."
        />
        <link rel="stylesheet" href="http://localhost:8000/css/style.css">
        <title>Contedia Form</title>
    </head>
    <body>
        <main id="main-content">