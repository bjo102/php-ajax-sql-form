<?php

/**
 * Contedia Views: Footer
 * 
 * @package Contedia\Views
 */

?>
</main>
<footer id="page-footer" class="section">
    <p id="ftr-cpy-rt-notice" class="copyright-notice">Copyright <?php echo date('Y'); ?> &copy; Byron O'Malley</p>
</footer>
<script src="http://localhost:8000/js/main.mjs" type="module"></script>
</body>
</html>