<?php

/**
 * Contedia Views: Form
 * 
 * @package Contedia\Views
 */

?>

<header id="form-header" class="section">
    <div class="section-wrap">
        <h1 id="form-heading">Contedia Form</h1>
        <p id="form-description">
            You can make submissions on this form which will be sent to the 
            <strong>"<?php echo DB_NAME; ?>"</strong> database.
        </p>
        <p>
            You can see these submissions in phpMyAdmin: go to <a href="http://localhost:8080/index.php" target="_blank"><strong>http://localhost:8080/index.php</strong></a> and login with
            <strong>"<?php echo DB_USER; ?>"</strong> as the username and <strong>"<?php echo DB_PASS; ?>"</strong> as the password. You shouldnt have to worry about the "Server" field.
        </p>
    </div>

</header>

<section class="section">
    <div class="section-wrap">
        <form id="contedia-form" method="post" aria-labelledby="form-heading" aria-describedby="form-description">
            <div class="form-body">

                <div class="field-container width-half required">
                    <label for="first-name">First Name</label>
                    <input id="first-name" name="first_name" type="text" required>
                    <p id="err-note-first_name" class="err-note"></p>
                </div>

                <div class="field-container width-half">
                    <label for="last-name">Last Name</label>
                    <input id="last-name" name="last_name" type="text">
                    <p id="err-note-last_name" class="err-note"></p>
                </div>

                <div class="field-container required">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="email" required>
                    <p id="err-note-email" class="err-note"></p>
                </div>

                <div class="field-container width-half required">
                    <label for="subject">Subject</label>
                    <input id="subject" name="subject" type="text" >
                    <p id="err-note-subject" class="err-note"></p>
                </div>

                <div class="field-container width-half required">
                    <label for="telephone-no">Telephone no.</label>
                    <input id="telephone-no" name="telephone_no" type="text" required>
                    <p id="err-note-telephone_no" class="err-note"></p>
                </div>

                <div class="field-container width-half required">
                    <div id="cars-select-wrapper" class="select-wrapper">
                        <p class="field-label" aria-label>Car</p>
                        <div class="select">
                            <div class="select__trigger">
                                <span>Tesla</span>
                                <div class="arrow"></div>
                            </div>
                            <div class="custom-options">
                                <span class="custom-option selected" data-value="tesla">Tesla</span>
                                <span class="custom-option" data-value="volvo">Volvo</span>
                                <span class="custom-option" data-value="mercedes">Mercedes</span>
                            </div>
                        </div>
                    </div>
                    <p id="err-note-car" class="err-note"></p>
                </div>

                <div class="field-container width-half required">
                    <div id="motorbike-select-wrapper" class="select-wrapper">
                        <p class="field-label" aria-label>Motorbike</p>
                        <div class="select">
                            <div class="select__trigger">
                                <span>Harley</span>
                                <div class="arrow"></div>
                            </div>
                            <div class="custom-options">
                                <span class="custom-option selected" data-value="harley">Harley</span>
                                <span class="custom-option" data-value="triumph">Triumph</span>
                                <span class="custom-option" data-value="suzuki">Suzuki</span>
                            </div>
                        </div>
                    </div>
                    <p id="err-note-bike" class="err-note"></p>
                </div>

                <div class="field-container radio-field-container required">
                    <p class="field-label" aria-label>Fuel</p>
                    <div class="radio-btns-container">
                        <div class="radio-btn-container">
                            <input id="petrol-fuel" type="radio" name="fuel" value="petrol">
                            <label for="petrol-fuel">Petrol</label>
                        </div>

                        <div class="radio-btn-container">
                            <input id="diesel-fuel" type="radio" name="fuel" value="diesel">
                            <label for="diesel-fuel">Diesel</label>
                        </div>

                        <div class="radio-btn-container">
                            <input id="kerosene-fuel" type="radio" name="fuel" value="kerosene">
                            <label for="kerosene-fuel">Kerosene</label>
                        </div>

                        <div class="radio-btn-container">
                            <input id="cooking-oil-fuel" type="radio" name="fuel" value="cooking-oil">
                            <label for="cooking-oil-fuel">Cooking Oil</label>
                        </div>
                    </div>
                
                    <p id="err-note-fuel" class="err-note"></p>
                </div>

                <div class="field-container">
                    <label for="comment">Comment</label>
                    <textarea id="comment" name="comment"></textarea>
                    <p id="err-note-comment" class="err-note"></p>
                </div>

                <div class="field-container checkbox-field-container required">
                    <input id="accept-gdpr" name="accept_gdpr" type="checkbox" required>
                    <label for="accept-gdpr">I consent to having this website store my submitted infromation so they can respond to my inquiry.</label>
                    <p id="err-note-gdpr" class="err-note"></p>
                </div>

            </div>

            <footer class="form-footer">
                <input id="contedia-form-submit" name="contedia_form_submit" type="submit" value="Submit">
            </footer>
        </form>
    </div>
</section>