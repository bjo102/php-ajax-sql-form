<?php

/**
 * Contedia Classes: Submission
 * 
 * @package Contedia\Classes
 */

/**
 * The Submissions model.
 */
class Submission {

    private $db;
    
    public function __construct()
    {
        $this->db = new DB;
    }

    /**
     * Add a submission to the database.
     */
    public function add_sub(array $data)
    {
        $this->db->query(
            'INSERT
            INTO submissions (first_name, last_name, email, subject, telephone_no, car, bike, fuel, comment, gdpr)
            VALUES(:first_name, :last_name, :email, :subject, :telephone_no, :car, :bike, :fuel, :comment, :gdpr)'
        );
        $this->db->bind(':first_name',      $data['first_name']);
        $this->db->bind(':last_name',       $data['last_name']);
        $this->db->bind(':email',           $data['email']);
        $this->db->bind(':subject',         $data['subject']);
        $this->db->bind(':telephone_no',    $data['telephone_no']);
        $this->db->bind(':car',             $data['car']);
        $this->db->bind(':bike',            $data['bike']);
        $this->db->bind(':fuel',            $data['fuel']);
        $this->db->bind(':comment',         $data['comment']);
        $this->db->bind(':gdpr',            $data['gdpr']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}