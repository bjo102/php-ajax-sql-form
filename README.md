# Contedia Form

This a system for submissions to an SQL database via AJAX POST requests, which runs on three docker containers:

- PHP/Apache
- MySQL
- phpMyAdmin

Please see `docker-compose.yml` files for details.

## Create an .env file

    PHP_PORT=80
    MYSQL_ROOT_PASSWORD=root102toor
    MYSQL_DATABASE=contedia_submissions
    MYSQL_USER=contdbusr
    MYSQL_PASSWORD=contdbpass

Then run `docker-compose up -d --build` and you should be able to see the form at http://localhost:8000/, and phpMyAmin at http://localhost:8080/index.php, you can login there with **MYSQL_USER** and **MYSQL_PASS** credentials found in the `.env` file. You shouldn't  need to fill in the **"Server"** field.

You should run the import to the database fist to initiate the table for submissions. There is the `submissions.sql` file, which will create the "submissions" table.

## Notable files

The volume for the php docker service is the `app/` directory, so file paths are relative to this.

- **The form:** `src/theme/form.php`
- **Form behaviour:** `public/js/*`
- **AJAX POST Request handling:** `src/api.php`
- **Database Updates:** `src/classes/DB|Submissions.php`